package com.example.melaliemealdeliveryapp.apiservice

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiClient {
    private var retrofit: Retrofit? = null
    private var username: String? = "1"
    private var password: String? = "hungry12345678"

    fun getRetrofitInstance(): Retrofit? {
        val interceptor = BasicAuthInterceptor(username!!, password!!)
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
        }
        return retrofit
    }

//    private var retrofit: Retrofit? = null
//    val client: Retrofit?
//        get() {
//            val interceptor = HttpLoggingInterceptor()
//            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
//            val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
//            retrofit = Retrofit.Builder()
//                .baseUrl("https://maps.googleapis.com/maps/api/")
//                .addConverterFactory(GsonConverterFactory.create())
//                .client(client)
//                .build()
//            return retrofit
}