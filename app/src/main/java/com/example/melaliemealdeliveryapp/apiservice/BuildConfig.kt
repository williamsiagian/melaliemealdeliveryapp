package com.example.melaliemealdeliveryapp.apiservice

object BuildConfig {
    val BASE_URL = "https://melalie-meal-delivery.herokuapp.com/"
}