package com.example.melaliemealdeliveryapp.apiservice

import com.example.melaliemealdeliveryapp.model.NearestRestaurant
import com.example.melaliemealdeliveryapp.model.PopularRestaurant
import com.example.melaliemealdeliveryapp.model.RestaurantMenu
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {
    @Headers(*["Content-Type: application/json"])
    @GET("restaurant-finder/open/")
    open fun getOpenRestaurant(@Query("timestamp") timestamp: String?): Call<ArrayList<NearestRestaurant?>?>?

    @Headers(*["Content-Type: application/json"])
    @GET("restaurant-finder/nearest/{latitude}/{longitude}")
    fun getNearestRestaurant(
        @Path("latitude") latitude: Double?,
        @Path("longitude") longitude: Double?,
    ): Call<ArrayList<NearestRestaurant?>?>?

    @Headers(*["Content-Type: application/json"])
    @GET("restaurant-finder/top-restaurant/{sort-type}/{limit}")
    fun getPopularRestaurant(
        @Path("sort-type") sortType: String?,
        @Path("limit") limit: Int?,
    ): Call<ArrayList<PopularRestaurant?>?>?

    @Headers(*["Content-Type: application/json"])
    @GET("restaurants/{id}/menus")
    fun getRestaurantMenus(
        @Path("id") id: Int?,
    ): Call<ArrayList<RestaurantMenu?>?>?
}