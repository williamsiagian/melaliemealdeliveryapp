package com.example.melaliemealdeliveryapp

import android.app.AlertDialog
import android.content.DialogInterface
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.melaliemealdeliveryapp.model.NearestRestaurant
import com.example.melaliemealdeliveryapp.model.PopularRestaurant
import com.example.melaliemealdeliveryapp.ui.HomePresenter
import com.example.melaliemealdeliveryapp.ui.HomeView
import com.example.melaliemealdeliveryapp.ui.NearestRestaurantAdapter
import com.example.melaliemealdeliveryapp.ui.PopularRestaurantAdapter
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.*


class HomeFragment : Fragment(), HomeView.InitView, NearestRestaurantAdapter.Listener, PopularRestaurantAdapter.Listener {

    private var latitude: Double? = null
    private var longitude: Double? = null
    var adapterNearest: NearestRestaurantAdapter? = null
    var adapterPopular: PopularRestaurantAdapter? = null
    var nearestRestaurants: ArrayList<NearestRestaurant?>? = null
    var popularRestaurants: ArrayList<PopularRestaurant?>? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val presenter = HomePresenter(this)
        adapterNearest = NearestRestaurantAdapter()
        adapterPopular = PopularRestaurantAdapter()

//        searchView.setOnQueryTextListener(object : android.widget.SearchView.OnQueryTextListener {
//            override fun onQueryTextSubmit(query: String?): Boolean {
//                return false;
//            }
//
//            override fun onQueryTextChange(newText: String?): Boolean {
//                return false;
//            }
//        })

        adapterNearest?.listener = this
        adapterPopular?.listener = this
        val manager = LinearLayoutManager(context)
        recyclerViewRestaurant.layoutManager = manager
        val dividerItemDecoration = androidx.recyclerview.widget.DividerItemDecoration(context, (manager).orientation)
        recyclerViewRestaurant?.addItemDecoration(dividerItemDecoration)

        arguments?.let {
            latitude = it.getDouble(ARG_LATITUDE)
            longitude = it.getDouble(ARG_LONGITUDE)
        }

        Log.d("latitudee", latitude.toString())
        Log.d("longitude", longitude.toString())
        getCompleteAddress(latitude, longitude)

        presenter.getNearestRestaurantsList(latitude, longitude)

        btnPopular.setOnClickListener {
            val dialogClickListener = DialogInterface.OnClickListener { dialog, which ->
                when (which) {
                    DialogInterface.BUTTON_POSITIVE -> {
                        presenter.getPopularRestaurantsList("transaction-number", 10)
                        dialog.dismiss()
                    }
                    DialogInterface.BUTTON_NEGATIVE -> {
                        presenter.getPopularRestaurantsList("total-amount", 10)
                        dialog.dismiss()
                    }
                }
            }

            val builder = AlertDialog.Builder(context)
            builder.setMessage("Choose Sort Type").setPositiveButton("transaction-number", dialogClickListener)
                    .setNegativeButton("total-amount", dialogClickListener).show()
        }

        btnNearby.setOnClickListener {
            presenter.getNearestRestaurantsList(latitude, longitude)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun showLoading() {
        progressBar?.visibility = View.VISIBLE
        recyclerViewRestaurant?.visibility = View.GONE
    }

    override fun hideLoading() {
        progressBar?.visibility = View.GONE
        recyclerViewRestaurant?.visibility = View.VISIBLE
    }

    override fun nearestRestaurantList(restaurants: ArrayList<NearestRestaurant?>?) {
        this.nearestRestaurants = restaurants
        adapterNearest?.restaurants = restaurants
        recyclerViewRestaurant?.setAdapter(adapterNearest)
        adapterNearest?.notifyDataSetChanged()
    }

    override fun popularRestaurantList(restaurants: ArrayList<PopularRestaurant?>?) {
        this.popularRestaurants = restaurants
        adapterPopular?.restaurants = restaurants
        recyclerViewRestaurant?.setAdapter(adapterPopular)
        adapterPopular?.notifyDataSetChanged()
    }

    fun getCompleteAddress(latitude: Double?, longitude: Double?){
        var geocoder: Geocoder?
        var addresses: List<Address>?

        geocoder = Geocoder(context, Locale.getDefault())

        addresses = latitude?.let {
            longitude?.let { it1 ->
                geocoder.getFromLocation(
                        it,
                        it1,
                        1
                )
            }
        } // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        val address = addresses!![0]
            .getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

        textViewCurrentLoc.text = address
    }

    companion object {
        private const val ARG_LATITUDE = "latitude"
        private const val ARG_LONGITUDE = "longitude"

        fun newInstance(latitude: Double, longitude: Double) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putDouble(ARG_LATITUDE, latitude)
                    putDouble(ARG_LONGITUDE, longitude)
                }
            }
    }

    override fun onClick(restaurant: NearestRestaurant) {
        startActivity(context?.let { DetailActivity?.getIntent(it, restaurant) })
    }

    override fun onClick(restaurant: PopularRestaurant) {
        startActivity(context?.let { DetailActivity?.getIntent(it, restaurant) })
    }

}
