package com.example.melaliemealdeliveryapp

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.*

class MainActivity : AppCompatActivity() {

    lateinit var fUsedLocationProviderClient : FusedLocationProviderClient
    lateinit var currentLocation: Location

    var latitude: Double? = null
    var longitude: Double? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fUsedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        fetchLastLocation()
    }

    fun fetchLastLocation(){
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            getCurrentLocation()
        }else{
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 10)
        }
    }

    @SuppressLint("MissingPermission")
    fun getCurrentLocation(){
        var task : Task<Location> = fUsedLocationProviderClient.lastLocation
        task.addOnSuccessListener(object : OnSuccessListener<Location> {
            override fun onSuccess(location: Location?) {
                if (location != null) {
                    currentLocation = location
                    latitude = currentLocation.latitude
                    longitude = currentLocation.longitude

                    Log.d("latitudee", latitude.toString())
                    Log.d("longitudeee", longitude.toString())

                    setCurrentFragment(HomeFragment.newInstance(latitude!!, longitude!!))

                    bottomNavigationView.setOnNavigationItemSelectedListener {
                        when(it.itemId){
                            R.id.home-> longitude?.let { it1 ->
                                latitude?.let { it2 ->
                                    HomeFragment.newInstance(
                                        it2,
                                        it1
                                    )
                                }
                            }?.let { it2 -> setCurrentFragment(it2) }
                            R.id.order->setCurrentFragment(OrderFragment())
                            R.id.account->setCurrentFragment(AccountFragment())
                        }
                        true
                    }
                }
            }

        })
    }

    private fun setCurrentFragment(fragment: Fragment)=
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.flFragment,fragment)
                commit()
            }
}