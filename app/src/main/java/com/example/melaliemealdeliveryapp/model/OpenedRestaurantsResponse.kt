package com.example.melaliemealdeliveryapp.model

import com.google.gson.annotations.SerializedName

class OpenedRestaurantsResponse {
    @SerializedName("id")
    var id: Int? = 0
    @SerializedName("name")
    var name: String? = null
    @SerializedName("latitude")
    var latitude: Double? = 0.0
    @SerializedName("longitude")
    var longitude: Double? = 0.0
    @SerializedName("balance")
    var balance: Double? = 0.0
}