package com.example.melaliemealdeliveryapp.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class PopularRestaurant: Serializable {
    @SerializedName("id")
    var id: Int? = 0
    @SerializedName("name")
    var name: String? = null
    @SerializedName("latitude")
    var latitude: Double? = 0.0
    @SerializedName("longitude")
    var longitude: Double? = 0.0
    @SerializedName("transaction_number")
    var transactionNumber: Int? = 0
    @SerializedName("total_amount")
    var totalAmount: Double? = 0.0
}