package com.example.melaliemealdeliveryapp.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class RestaurantMenu: Serializable {
    @SerializedName("id")
    var id: Int? = 0
    @SerializedName("restaurant_id")
    var restaurantId: Int? = null
    @SerializedName("name")
    var name: String? = "0.0"
    @SerializedName("price")
    var price: Double? = 0.0
}