package com.example.melaliemealdeliveryapp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.melaliemealdeliveryapp.model.NearestRestaurant
import com.example.melaliemealdeliveryapp.model.PopularRestaurant
import com.example.melaliemealdeliveryapp.model.RestaurantMenu
import com.example.melaliemealdeliveryapp.ui.DetailPresenter
import com.example.melaliemealdeliveryapp.ui.DetailView
import com.example.melaliemealdeliveryapp.ui.MenuAdapter
import kotlinx.android.synthetic.main.activity_detail.*


class DetailActivity : AppCompatActivity(), DetailView.InitView {
    var adapter: MenuAdapter? = null
    var menus: ArrayList<RestaurantMenu?>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val presenter = DetailPresenter(this)
        adapter = MenuAdapter()

        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val bundle = intent.extras
        if (bundle == null) {
            Toast.makeText(applicationContext, "Bundle Null", Toast.LENGTH_SHORT).show()
            finish()
            return
        }

        val restaurant = bundle.get(KEY_RESTAURANT) as? NearestRestaurant
        val id = restaurant?.id

        textViewRestaurantName.text = restaurant?.name
        textViewDistance.text = restaurant?.distance.toString()

        val manager = GridLayoutManager(this, 2)
        recyclerViewMenu.layoutManager = manager

        presenter.getRestaurantMenu(id)
        Log.d("idiii", id.toString())
    }

    companion object{
        private val KEY_RESTAURANT = "DetailActivity.restaurant"

        fun getIntent(context: Context, restaurant: NearestRestaurant?): Intent {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra(KEY_RESTAURANT, restaurant)

            return intent
        }

        fun getIntent(context: Context, restaurant: PopularRestaurant?): Intent {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra(KEY_RESTAURANT, restaurant)

            return intent
        }
    }

    override fun showLoading() {
        progressBar?.visibility = View.VISIBLE
        recyclerViewMenu?.visibility = View.GONE
    }

    override fun hideLoading() {
        progressBar?.visibility = View.GONE
        recyclerViewMenu?.visibility = View.VISIBLE
    }

    override fun restaurantMenus(menus: ArrayList<RestaurantMenu?>?) {
        this.menus = menus
        adapter?.menus = menus
        recyclerViewMenu?.setAdapter(adapter)
        adapter?.notifyDataSetChanged()
    }
}
