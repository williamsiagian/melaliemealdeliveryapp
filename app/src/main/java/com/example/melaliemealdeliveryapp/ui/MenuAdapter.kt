package com.example.melaliemealdeliveryapp.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.melaliemealdeliveryapp.R
import com.example.melaliemealdeliveryapp.model.NearestRestaurant
import com.example.melaliemealdeliveryapp.model.RestaurantMenu
import kotlinx.android.synthetic.main.row_restaurant_menu.view.*
import java.util.ArrayList

class MenuAdapter : RecyclerView.Adapter<MenuAdapter.ViewHolder>() {
    var menus: ArrayList<RestaurantMenu?>? = null
        set(menus){
            field = menus
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.row_restaurant_menu, null))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(getItems(position))

    fun getItems(position: Int): RestaurantMenu? = menus?.get(position)

    override fun getItemCount(): Int = menus?.size ?: 0

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: RestaurantMenu?) = with(itemView) {

            textViewName.text = item?.name
            textViewPrice.text = item?.price.toString()
        }
    }
}