package com.example.melaliemealdeliveryapp.ui

import com.example.melaliemealdeliveryapp.apiservice.ApiClient
import com.example.melaliemealdeliveryapp.apiservice.ApiInterface
import com.example.melaliemealdeliveryapp.model.RestaurantMenu
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailPresenter(initView: DetailView.InitView) : DetailView.GetRestaurantMenu {
    private val initView: DetailView.InitView

    init {
        this.initView = initView
    }

    override fun getRestaurantMenu(restaurantId: Int?) {
        initView.showLoading()
        val apiInterface: ApiInterface? = ApiClient().getRetrofitInstance()?.create(ApiInterface::class.java)
        val call: Call<ArrayList<RestaurantMenu?>?>? = apiInterface?.getRestaurantMenus(restaurantId)
        call?.enqueue(object : Callback<ArrayList<RestaurantMenu?>?> {

            override fun onResponse(call: Call<ArrayList<RestaurantMenu?>?>, response: Response<ArrayList<RestaurantMenu?>?>) {
                initView.hideLoading()
                initView.restaurantMenus(response.body())
            }

            override fun onFailure(call: Call<ArrayList<RestaurantMenu?>?>, t: Throwable) {
                initView.hideLoading()
                t.printStackTrace()
            }
        })
    }
}