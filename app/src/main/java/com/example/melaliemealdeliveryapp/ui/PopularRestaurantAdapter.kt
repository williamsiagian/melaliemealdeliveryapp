package com.example.melaliemealdeliveryapp.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.melaliemealdeliveryapp.R
import com.example.melaliemealdeliveryapp.model.NearestRestaurant
import com.example.melaliemealdeliveryapp.model.PopularRestaurant
import kotlinx.android.synthetic.main.row_popular_restaurant.view.*
import kotlinx.android.synthetic.main.row_restaurant.view.*
import kotlinx.android.synthetic.main.row_restaurant.view.textViewTitle
import java.util.ArrayList

class PopularRestaurantAdapter: RecyclerView.Adapter<PopularRestaurantAdapter.ViewHolder>() {
    var listener: Listener? = null
    var restaurants: ArrayList<PopularRestaurant?>? = null
        set(restaurants){
            field = restaurants
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.row_popular_restaurant, null))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(getItems(position), listener)

    fun getItems(position: Int): PopularRestaurant? = restaurants?.get(position)

    override fun getItemCount(): Int = restaurants?.size ?: 0

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(restaurant: PopularRestaurant?, listener: Listener?) = with(itemView) {
            textViewTransactionNumber.text = restaurant?.transactionNumber.toString()
            textViewTotalAmount.text = restaurant?.totalAmount.toString()
            itemView.setOnClickListener {
                listener?.onClick(restaurant!!)
            }
        }
    }

    interface Listener{
        fun onClick(restaurant: PopularRestaurant)
    }
}