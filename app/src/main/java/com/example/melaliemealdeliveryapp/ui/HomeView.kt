package com.example.melaliemealdeliveryapp.ui

import com.example.melaliemealdeliveryapp.model.NearestRestaurant
import com.example.melaliemealdeliveryapp.model.PopularRestaurant

interface HomeView {
    interface InitView {
        fun showLoading()
        fun hideLoading()
        fun nearestRestaurantList(restaurants: ArrayList<NearestRestaurant?>?)
        fun popularRestaurantList(restaurants: ArrayList<PopularRestaurant?>?)
    }

    interface GetRestaurants {
        fun getNearestRestaurantsList(latitude: Double?, longitude: Double?)
        fun getPopularRestaurantsList(sortType: String?, limit: Int?)
    }
}