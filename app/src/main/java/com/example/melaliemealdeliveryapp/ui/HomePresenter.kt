package com.example.melaliemealdeliveryapp.ui

import com.example.melaliemealdeliveryapp.apiservice.ApiClient
import com.example.melaliemealdeliveryapp.apiservice.ApiInterface
import com.example.melaliemealdeliveryapp.model.NearestRestaurant
import com.example.melaliemealdeliveryapp.model.PopularRestaurant
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomePresenter(initView: HomeView.InitView) : HomeView.GetRestaurants {
    private val initView: HomeView.InitView

    init {
        this.initView = initView
    }

    override fun getNearestRestaurantsList(latitude: Double?, longitude: Double?) {
        initView.showLoading()
        val apiInterface: ApiInterface? = ApiClient().getRetrofitInstance()?.create(ApiInterface::class.java)
        val call: Call<ArrayList<NearestRestaurant?>?>? = apiInterface?.getNearestRestaurant(latitude, longitude)
        call?.enqueue(object : Callback<ArrayList<NearestRestaurant?>?> {

            override fun onResponse(call: Call<ArrayList<NearestRestaurant?>?>, response: Response<ArrayList<NearestRestaurant?>?>) {
                initView.hideLoading()
                initView.nearestRestaurantList(response.body())
            }

            override fun onFailure(call: Call<ArrayList<NearestRestaurant?>?>, t: Throwable) {
                initView.hideLoading()
                t.printStackTrace()
            }
        })
    }

    override fun getPopularRestaurantsList(sortType: String?, limit: Int?) {
        initView.showLoading()
        val apiInterface: ApiInterface? = ApiClient().getRetrofitInstance()?.create(ApiInterface::class.java)
        val call: Call<ArrayList<PopularRestaurant?>?>? = apiInterface?.getPopularRestaurant(sortType, limit)
        call?.enqueue(object : Callback<ArrayList<PopularRestaurant?>?> {

            override fun onResponse(call: Call<ArrayList<PopularRestaurant?>?>, response: Response<ArrayList<PopularRestaurant?>?>) {
                initView.hideLoading()
                initView.popularRestaurantList(response.body())
            }

            override fun onFailure(call: Call<ArrayList<PopularRestaurant?>?>, t: Throwable) {
                initView.hideLoading()
                t.printStackTrace()
            }
        })
    }
}