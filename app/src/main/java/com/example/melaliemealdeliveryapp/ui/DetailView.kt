package com.example.melaliemealdeliveryapp.ui

import com.example.melaliemealdeliveryapp.model.RestaurantMenu

interface DetailView {
    interface InitView {
        fun showLoading()
        fun hideLoading()
        fun restaurantMenus(menus: ArrayList<RestaurantMenu?>?)
    }

    interface GetRestaurantMenu{
        fun getRestaurantMenu(restaurantId: Int?)
    }
}