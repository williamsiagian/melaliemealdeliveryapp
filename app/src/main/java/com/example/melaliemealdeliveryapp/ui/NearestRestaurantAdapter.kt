package com.example.melaliemealdeliveryapp.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.melaliemealdeliveryapp.R
import com.example.melaliemealdeliveryapp.model.NearestRestaurant
import kotlinx.android.synthetic.main.row_restaurant.view.*
import java.util.*

class NearestRestaurantAdapter : RecyclerView.Adapter<NearestRestaurantAdapter.ViewHolder>() {
    var listener: Listener? = null
    var restaurants: ArrayList<NearestRestaurant?>? = null
        set(restaurants){
            field = restaurants
            notifyDataSetChanged()
        }

    fun addRestaurants(items: ArrayList<NearestRestaurant?>?){
        if (this.restaurants == null) {
            this.restaurants = ArrayList()
        }
        items?.let { this.restaurants?.addAll(it) }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.row_restaurant, null))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(getItems(position), listener)

    fun getItems(position: Int): NearestRestaurant? = restaurants?.get(position)

    override fun getItemCount(): Int = restaurants?.size ?: 0

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(restaurant: NearestRestaurant?, listener: Listener?) = with(itemView) {

            textViewTitle.text = restaurant?.name
            itemView.setOnClickListener {
                listener?.onClick(restaurant!!)
            }
        }
    }

    interface Listener{
        fun onClick(restaurant: NearestRestaurant)
    }
}